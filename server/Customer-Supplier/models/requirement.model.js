'use strict';
const bcrypt 			= require('bcrypt');
const bcrypt_p 			= require('bcrypt-promise');
const jwt           	= require('jsonwebtoken');
const {TE, to}          = require('../services/util.service');
const CONFIG            = require('../config/config');

module.exports = (sequelize, DataTypes) => {
    var Model = sequelize.define('Requirement', {
        user_id : DataTypes.INTEGER,
        fuel_mix_renewable : DataTypes.STRING,
        fuel_mix_local : DataTypes.STRING,
        rate_type  : DataTypes.STRING,
        onsite_generation : DataTypes.STRING,
        quote_type : DataTypes.STRING,
        contract_duration : DataTypes.INTEGER,
    });

    Model.prototype.toWeb = function (pw) {
        let json = this.toJSON();
        return json;
    };

    return Model;
};