'use strict';
const bcrypt 			= require('bcrypt');
const bcrypt_p 			= require('bcrypt-promise');
const jwt           	= require('jsonwebtoken');
const {TE, to}          = require('../services/util.service');
const CONFIG            = require('../config/config');

module.exports = (sequelize, DataTypes) => {
    var Model = sequelize.define('Customer', {
        user_id : DataTypes.INTEGER,
        customer_name : DataTypes.STRING,
        contact_name : DataTypes.STRING,
        address1 : DataTypes.STRING,
        address2 : DataTypes.STRING,
        city : DataTypes.STRING,
        state : DataTypes.STRING,
        zip : DataTypes.INTEGER,
        country : DataTypes.INTEGER,
        customer_approx_monthly_bill : DataTypes.INTEGER,
        customer_phone : {type: DataTypes.INTEGER, allowNull: true},
        website : DataTypes.STRING,
        federal_tax_id : DataTypes.INTEGER,
    });

    Model.prototype.toWeb = function (pw) {
        let json = this.toJSON();
        return json;
    };

    return Model;
};