'use strict';
const bcrypt 			= require('bcrypt');
const bcrypt_p 			= require('bcrypt-promise');
const jwt           	= require('jsonwebtoken');
const {TE, to}          = require('../services/util.service');
const CONFIG            = require('../config/config');

module.exports = (sequelize, DataTypes) => {
    var Model = sequelize.define('Supplier', {
        user_id : DataTypes.INTEGER,
        auth_type : DataTypes.STRING,
        auth_agency : DataTypes.STRING,
        auth_no : DataTypes.STRING,
        auth_expiry_date : DataTypes.STRING,
        auth_certificate : DataTypes.STRING,
        supplier_company_name : DataTypes.STRING,
        contact_name : DataTypes.STRING,
        address1 : DataTypes.STRING,
        address2 : DataTypes.STRING,
        city : DataTypes.STRING,
        state : DataTypes.STRING,
        zip : DataTypes.INTEGER,
        country : DataTypes.INTEGER,
        supplier_phone  : {type: DataTypes.INTEGER, allowNull: true},
        website : DataTypes.STRING,
        federal_tax_id : DataTypes.INTEGER,
    });

    Model.prototype.toWeb = function (pw) {
        let json = this.toJSON();
        return json;
    };

    return Model;
};