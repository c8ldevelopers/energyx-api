const { Requirement } 	    = require('../models');
const validator     = require('validator');
const { to, TE }    = require('../services/util.service');
const { requirementCreate }    = require('../repositories/requirement.repository');

const createRequirement = async (productInfo) => {
    let  err;

    if(validator.isEmpty(productInfo.fuel_mix_renewable)) {
        TE('Please select fuel mix renewable ');
    }else if(validator.isEmpty(productInfo.fuel_mix_local)){
        TE('Please select fuel mix local');
    }else if(validator.isEmpty(productInfo.rate_type)){
        TE('Please select rate type');
    }else if(validator.isEmpty(productInfo.onsite_generation)){
        TE('Please select on side generation');
    }else if(validator.isEmpty(productInfo.quote_type)){
        TE('Please select quote type');
    }else if(!validator.isNumeric(productInfo.contract_duration)){
        TE('Please enter contract duration');
    }else{
	    [err, requirement] = await to(requirementCreate(productInfo));
	    if(err) TE('Customer product not insert please check!');
	    return requirement;
	}
}
module.exports.createRequirement = createRequirement;