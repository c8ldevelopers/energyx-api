const { Supplier } 	    = require('../models');
const validator     = require('validator');
const { to, TE }    = require('../services/util.service');
const { supplierCreateProfile }    = require('../repositories/supplier.repository');
const Email = require('email-templates');
const nodemailer = require('nodemailer');

const createSupplierProfile = async (supplierProfileInfo) => {
    let err;

     if(!validator.isAlpha(supplierProfileInfo.supplier_company_name)) {
        TE('Please enter alphabets company name only ');
    }else if(!validator.isAlpha(supplierProfileInfo.contact_name)){
        TE('Please enter alphabets contact name only ');
    }else if(validator.isEmpty(supplierProfileInfo.address1)){
        TE('Please enter Address');
    }else if(!validator.isAlpha(supplierProfileInfo.city)){
        TE('Please enter city');
    }else if(!validator.isAlpha(supplierProfileInfo.state)){
        TE('Please enter state');
    }else if(!validator.isNumeric(supplierProfileInfo.zip)){
        TE('Please enter zip code');
    }else if(!validator.isAlpha(supplierProfileInfo.country)){
        TE('Please enter country');
    }else if(!validator.isNumeric(supplierProfileInfo.supplier_phone)){
        TE('Please enter supplier phone');
    }else{
	    [err, customer] = await to(supplierCreateProfile(supplierProfileInfo));
	    if(err) TE('customer profile data not insert. try again!');
	    return customer;
	}
}
module.exports.createSupplierProfile = createSupplierProfile;