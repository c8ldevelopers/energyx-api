const { Customer } 	    = require('../models');
const validator     = require('validator');
const { to, TE }    = require('../services/util.service');
const { customerCreateProfile }    = require('../repositories/customer.repository');
const Email = require('email-templates');
const nodemailer = require('nodemailer');

const createCustomerProfile = async (customerProfileInfo) => {
    let err;
    if(!validator.isAlpha(customerProfileInfo.customer_name)) {
        TE('Please enter alphabets customer name only ');
    }else if(!validator.isAlpha(customerProfileInfo.contact_name)){
        TE('Please enter alphabets contact name only ');
    }else if(validator.isEmpty(customerProfileInfo.address1)){
        TE('Please enter Address');
    }else if(!validator.isAlpha(customerProfileInfo.city)){
        TE('Please enter city');
    }else if(!validator.isAlpha(customerProfileInfo.state)){
        TE('Please enter state');
    }else if(!validator.isNumeric(customerProfileInfo.zip)){
        TE('Please enter zip code');
    }else if(!validator.isAlpha(customerProfileInfo.country)){
        TE('Please enter country');
    }else if(!validator.isNumeric(customerProfileInfo.customer_approx_monthly_bill)){
        TE('Please enter monthly bill');
    }else if(!validator.isNumeric(customerProfileInfo.customer_phone)){
        TE('Please enter customer phone');
    }else{
    	[err, customer] = await to(customerCreateProfile(customerProfileInfo));
	    if(err) TE('customer profile data not insert. try again!');
	    return customer;
    }
}
module.exports.createCustomerProfile = createCustomerProfile;