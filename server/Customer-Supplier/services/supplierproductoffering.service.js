const { supplierProductOfferingCreate }    = require('../repositories/supplierproductoffering.repository');
const { to, TE }    = require('../services/util.service');

const createSupplierProductOffering = async (supplierProductOfferingInfo) => {
		let  err;
	    [err, supplierProductOfferingInfo] = await to(supplierProductOfferingCreate(supplierProductOfferingInfo));
	    if(err) TE('Supplier product offering data not add. Please try again!');
	    return supplierProductOfferingInfo;
}
module.exports.createSupplierProductOffering = createSupplierProductOffering;