'use strict';

function supplierProductOfferingCreate(supplierProductOfferingInfo) {
 	 return new Promise(async function(resolve, reject) {
	    try {
			const Sequelize = require('sequelize');
			const sequelize = new Sequelize('energyx', 'root', '', { host: 'localhost', dialect: 'mysql' });
			sequelize.query("SELECT * FROM `products` WHERE title = '"+supplierProductOfferingInfo.product_name+"'", { type: sequelize.QueryTypes.SELECT})
			.then(product => {					
					var partner_deal = supplierProductOfferingInfo.partner_deals;
					if(partner_deal === 'yes'){
							var par_deal = 1;
					} else if(partner_deal === 'no'){
							var par_deal = 2;
					}
					sequelize.query("INSERT INTO supplier_product_offering (product_id ,custom_or_matrix_pricing,partner_deals) VALUES('"+product[0].id+"','"+supplierProductOfferingInfo.custom_or_matrix_pricing+"','"+par_deal+"')", { type: sequelize.QueryTypes.INSERT })
			    	.then(function (clientInsertId) {
			    			resolve(clientInsertId,201);
					});
			});   	
	    } catch (err) {
	     reject(err,422);
	    }
	});
}

module.exports.supplierProductOfferingCreate = supplierProductOfferingCreate;

