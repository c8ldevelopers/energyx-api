const { Customer }          = require('../models');
const customerProfileService       = require('../services/customer.service');
const { to, ReE, ReS }  = require('../services/util.service');

const create = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
            let user_id = req.headers['user_id'];
	    	var user_login_id = { 'user_id': user_id }
	    	var customer_profile_data = req.body;
	    	const body = Object.assign(user_login_id, customer_profile_data);
	        [err, customer] = await to(customerProfileService.createCustomerProfile(body));
	        if(err) return ReE(res, err, 422);
	        return ReS(res, {message:'Successfully created customer profile!', customer:customer.toWeb()}, 201);            
}	
module.exports.create = create;