const { Requirement }          = require('../models');
const  requirementService       = require('../services/requirement.service');
const { to, ReE, ReS }  = require('../services/util.service');

const create = async function(req, res){
    		 res.setHeader('Content-Type', 'application/json');
            let user_id = req.headers['user_id'];
	    	const requirement_data = { 
							    								'user_id': user_id,
							    								'fuel_mix_renewable': req.body.fuel_mix_renewable,
							    								'fuel_mix_local': req.body.fuel_mix_local,
							    								'rate_type': req.body.rate_type,
							    								'onsite_generation': req.body.onsite_generation,
							    								'quote_type': req.body.quote_type,
							    								'contract_duration': req.body.contract_duration
							    							};

			if(req.body.product_name == ''){	
					return ReE(res, 'Please enter a product name.');
			}else if(req.body.locality_zip == ''){
					return ReE(res, 'Please enter a locality zip number.');
			}else if(req.body.locality_power == ''){
					return ReE(res, 'Please enter a locality power.');
			}else if(req.body.locality_classification == ''){
					return ReE(res, 'Please select locality classification.');
			} else {
					[err, requirement] = await to(requirementService.createRequirement(requirement_data));
			        if(err) return ReE(res, err, 422);
			         
			        const Sequelize = require('sequelize');
					const sequelize = new Sequelize('energyx', 'root', '', { host: 'localhost', dialect: 'mysql' });
					sequelize.query("SELECT * FROM `products` WHERE title = '"+req.body.product_name+"'", { type: sequelize.QueryTypes.SELECT})
					.then(product => {
					    	sequelize.query("INSERT INTO requirement_locality (requirement_id,product_id,locality_zip,locality_power,locality_classification) VALUES('"+requirement.dataValues.id+"','"+product[0].id+"','"+req.body.locality_zip+"','"+req.body.locality_power+"','"+req.body.locality_classification+"')", { type: sequelize.QueryTypes.INSERT })
					    	.then(function (clientInsertId) {
					    		console.log(clientInsertId);
			                    return ReS(res, {message:'Successfully created customer requirement!', requirement:requirement.toWeb()}, 201);
							});
					 });   
			} 
}	
module.exports.create = create;