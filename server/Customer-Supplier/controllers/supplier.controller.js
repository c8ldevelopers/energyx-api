const { Supplier }          = require('../models');
const supplierProfileService       = require('../services/supplier.service');
const { to, ReE, ReS }  = require('../services/util.service');

const create = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
            let user_id = req.headers['user_id'];
	    	var user_login_id = { 'user_id': user_id }
	    	var supplier_profile_data = req.body;
	    	const body = Object.assign(user_login_id, supplier_profile_data);
	        [err, supplier] = await to(supplierProfileService.createSupplierProfile(body));
	        if(err) return ReE(res, err, 422);
	        return ReS(res, {message:'Successfully created supplier profile!', supplier:supplier.toWeb()}, 201);            
}	
module.exports.create = create;