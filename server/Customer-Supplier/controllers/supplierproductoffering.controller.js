const  supplierproductofferingService       = require('../services/supplierproductoffering.service');
const { to, ReE, ReS }  = require('../services/util.service');

const create = async function(req, res){
    		res.setHeader('Content-Type', 'application/json');
            let user_id = req.headers['user_id'];	        
		    var user_login_id = { 'user_id': user_id }
	        var supplier_product_offering_data = req.body;
	    	const body = Object.assign(user_login_id, supplier_product_offering_data);
	        [err] = await to(supplierproductofferingService.createSupplierProductOffering(body));
	        if(err) return ReE(res, err, 422);
	        return ReS(res, {message:'Successfully insert in supplier product offering data!'}, 201);            
			
}	
module.exports.create = create;