const express 			= require('express');
const router 			= express.Router();
const CustomerController = require('../controllers/customer.controller');
const RequirementController = require('../controllers/requirement.controller');
const SupplierController = require('../controllers/supplier.controller');
const SupplierProductOfferingController = require('../controllers/supplierproductoffering.controller');
const custom 	        = require('./../middleware/custom');
const passport      	= require('passport');
const path              = require('path');

require('./../middleware/passport')(passport)
router.get('/', function(req, res, next) {
  	res.json({status:"success", message:"Parcel Pending API", data:{"version_number":"v1.0.0"}})
});

/* Customer all routes */
router.post(    '/customers',     CustomerController.create);
router.post(    '/customers/products',        RequirementController.create);

/* Supplier all routes*/
router.post( '/suppliers',   SupplierController.create);
router.post('/suppliers/product_offering',  SupplierProductOfferingController.create);

module.exports = router;
