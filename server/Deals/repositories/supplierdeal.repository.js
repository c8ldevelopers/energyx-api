'use strict';
const { Deal } 	    = require('../models');

module.exports.supplierDealDataInsert = function(supplierDealInsertData){ 
    return Deal.create(supplierDealInsertData);
};

function supplierDealListing(user_id) {
 	 return new Promise(async function(resolve, reject) {
	    try {
			const Sequelize = require('sequelize');
			const sequelize = new Sequelize('energyx', 'root', '', { host: 'localhost', dialect: 'mysql' });
	    	sequelize.query("SELECT * FROM `deals` WHERE user_id = '"+user_id+"'", { type: sequelize.QueryTypes.SELECT})
				.then(dealList => {					
								resolve(dealList,201);
				});  
	    } catch (err) {
	     reject(err,422);
	    }
	});
}
module.exports.supplierDealListing = supplierDealListing;

function supplierDealDetailView(body) {
 	 return new Promise(async function(resolve, reject) {
	    try {
			const Sequelize = require('sequelize');
			const sequelize = new Sequelize('energyx', 'root', '', { host: 'localhost', dialect: 'mysql' });
	    	sequelize.query("SELECT * FROM `deals` WHERE user_id = '"+body.user_id+"' AND deal_number = '"+body.deal_number+"'", { type: sequelize.QueryTypes.SELECT})
				.then(dealDetails => {					
								resolve(dealDetails,201);
				});  
	    } catch (err) {
	     reject(err,422);
	    }
	});
}
module.exports.supplierDealDetailView = supplierDealDetailView;