'use strict';
const bcrypt 			= require('bcrypt');
const bcrypt_p 			= require('bcrypt-promise');
const jwt           	= require('jsonwebtoken');
const {TE, to}          = require('../services/util.service');
const CONFIG            = require('../config/config');

module.exports = (sequelize, DataTypes) => {
    var Model = sequelize.define('Deal', {
        user_id : DataTypes.INTEGER,
        contract_id : DataTypes.INTEGER,
        deal_number : DataTypes.STRING,
        deal_name : DataTypes.STRING,
        deal_description : DataTypes.STRING,
        status : DataTypes.STRING,
        deal_type : DataTypes.STRING,
        rate_type : DataTypes.STRING,
        term : DataTypes.STRING,
        expiry_date : DataTypes.STRING,
        price : {type: DataTypes.STRING, allowNull: true},
        cogs : DataTypes.STRING,
        total_expenses : DataTypes.STRING,
    });

    Model.prototype.toWeb = function (pw) {
        let json = this.toJSON();
        return json;
    };

    return Model;
};