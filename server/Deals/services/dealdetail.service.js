const { to, TE }    = require('../services/util.service');
const { dealDetailView }    = require('../repositories/dealdetail.repository');

const dealDetail = async function(body) {
        let  err;
        [err, dealDetails] = await to(dealDetailView(body));
        if(err) TE('Something is wrong. Please try again!');
        return dealDetails;
}
module.exports.dealDetail = dealDetail;