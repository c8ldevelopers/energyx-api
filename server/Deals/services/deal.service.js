const { to, TE }    = require('../services/util.service');
const { dealListing }    = require('../repositories/deal.repository');
const { dealDetailView } = require('../repositories/deal.repository');
const dealList = async function(user_id) {
        let  err;
        [err, deal] = await to(dealListing(user_id));
        if(err) TE('Something is wrong. Please try again!');
        return deal;
}
module.exports.dealList = dealList;

const dealDetail = async function(body) {
        let  err;
        [err, dealDetails] = await to(dealDetailView(body));
        if(err) TE('Something is wrong. Please try again!');
        return dealDetails;
}
module.exports.dealDetail = dealDetail;