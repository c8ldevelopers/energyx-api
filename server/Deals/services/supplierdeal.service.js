const { Deal } 	    = require('../models');
const validator     = require('validator');
const { to, TE }    = require('../services/util.service');
const { supplierDealDataInsert }    = require('../repositories/supplierdeal.repository');
const { supplierDealListing }    = require('../repositories/supplierdeal.repository');
const { supplierDealDetailView }    = require('../repositories/supplierdeal.repository');
const Email = require('email-templates');
const nodemailer = require('nodemailer');

const createSupplierDeals = async (supplierDealInsertData) => {
        let err;
    	[err, deals] = await to(supplierDealDataInsert(supplierDealInsertData));
	    if(err) TE('Supplier deal data not insert. try again!');
	    return deals;    
}
module.exports.createSupplierDeals = createSupplierDeals;

const supplierdealList = async function(user_id) {
        let  err;
        [err, supplierdeal] = await to(supplierDealListing(user_id));
        if(err) TE('Something is wrong. Please try again!');
        return supplierdeal;
}
module.exports.supplierdealList = supplierdealList;

const supplierdealDetail = async function(body) {
        let  err;
        [err, supplierdealDetails] = await to(supplierDealDetailView(body));
        if(err) TE('Something is wrong. Please try again!');
        return supplierdealDetails;
}
module.exports.supplierdealDetail = supplierdealDetail;