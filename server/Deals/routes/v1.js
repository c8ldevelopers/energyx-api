const express 			= require('express');
const router 			= express.Router();
const DealController = require('../controllers/deal.controller');
const SupplierDealController = require('../controllers/supplierdeal.controller')
const custom 	        = require('./../middleware/custom');
const passport      	= require('passport');
const path              = require('path');

require('./../middleware/passport')(passport)
router.get('/', function(req, res, next) {
  	res.json({status:"success", message:"Parcel Pending API", data:{"version_number":"v1.0.0"}})
});

/* Customer Deal  router */
router.get(       '/deals',            DealController.dealList);
router.post(    '/deals/deal_detail',   DealController.dealDetail);

/* Supplier Deal  router */
router.post(    '/supplier/deal_add',    SupplierDealController.supplierDealAdd);
router.get(       '/supplier/deals',           SupplierDealController.supplierDealList);
router.post(    '/supplier/deal_detail',  SupplierDealController.supplierDealDetail);

module.exports = router;
