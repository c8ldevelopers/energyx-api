const dealService       = require('../services/deal.service');
const { to, ReE, ReS }  = require('../services/util.service');

const dealList = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let user_id = req.headers['user_id'];
    [err, deal] = await to(dealService.dealList(user_id));
    if(err) return ReE(res, err, 422);
    return ReS(res, {dealList:deal}, 201);
}
module.exports.dealList = dealList;

const dealDetail = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    const body = {
    							'user_id': req.headers['user_id'],
    							'deal_number': req.body.deal_number    
    						};
    [err, dealDetails] = await to(dealService.dealDetail(body));
    if(err) return ReE(res, err, 422);
    return ReS(res, {dealDetails:dealDetails}, 201);
}
module.exports.dealDetail = dealDetail;