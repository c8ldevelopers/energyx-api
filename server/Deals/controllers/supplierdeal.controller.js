const { Deal }          = require('../models');
const  supplierDealService       = require('../services/supplierdeal.service');
const { to, ReE, ReS }  = require('../services/util.service');

const supplierDealAdd = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
            let user_id = req.headers['user_id'];
            var user_login_id = { 'user_id': user_id }
            var suppplier_deal_data = req.body;
            const body = Object.assign(user_login_id, suppplier_deal_data);
            [err, deal] = await to(supplierDealService.createSupplierDeals(body));
            if(err) return ReE(res, err, 422);
            return ReS(res, {message:'Successfully deal data insert!', deal:deal.toWeb()}, 201);            
}   
module.exports.supplierDealAdd = supplierDealAdd;

const supplierDealList = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let user_id = req.headers['user_id'];
    [err, supplierdeal] = await to(supplierDealService.supplierdealList(user_id));
    if(err) return ReE(res, err, 422);
    return ReS(res, {supplierdeal:supplierdeal}, 201);
}
module.exports.supplierDealList = supplierDealList;

const supplierDealDetail = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    const body = {
                                'user_id': req.headers['user_id'],
                                'deal_number': req.body.deal_number    
                            };
    [err, supplierDealDetails] = await to(supplierDealService.supplierdealDetail(body));
    if(err) return ReE(res, err, 422);
    return ReS(res, {supplierDealDetails:supplierDealDetails}, 201);
}
module.exports.supplierDealDetail = supplierDealDetail;
