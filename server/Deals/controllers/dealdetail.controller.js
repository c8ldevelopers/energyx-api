const dealdetailService       = require('../services/dealdetail.service');
const { to, ReE, ReS }  = require('../services/util.service');

const dealDetail = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let user_id = req.user.id;
    const body = {
    							'user_id': user_id,
    							'deal_number': req.body.deal_number    
    						};
    [err, dealDetails] = await to(dealdetailService.dealDetail(body));
    if(err) return ReE(res, err, 422);
    return ReS(res, {dealDetails:dealDetails}, 201);
}
module.exports.dealDetail = dealDetail;
