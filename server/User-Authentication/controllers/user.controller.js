const { User }          = require('../models');
const userService       = require('../services/user.service');
const { to, ReE, ReS }  = require('../services/util.service');

const create = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    const body = req.body;

    if(!body.unique_key && !body.email && !body.phone){
        return ReE(res, 'Please enter an email or phone number to register.');
    } else if(!body.password){
        return ReE(res, 'Please enter a password to register.');
    }else{
        let err, user;

        [err, user] = await to(userService.createUser(body));

        if(err) return ReE(res, err, 422);
        return ReS(res, {message:'Successfully created new user.', user:user.toWeb(), token:user.getJWT()}, 201);
    }
}
module.exports.create = create;

const get = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let user = req.user;
    return ReS(res, {user:user.toWeb()});
}
module.exports.get = get;


const login = async function(req, res){
    const body = req.body;
    let err, user;
    req.body.role = 0;
    console.log(req.body);
    [err, user] = await to(userService.authUser(req.body));
    if(err) return ReE(res, err, 422);

    return ReS(res, {message:'Successfully user login.', token:user.getJWT(), user:user.toWeb()});
}
module.exports.login = login;

