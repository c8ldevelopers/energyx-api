const express 			= require('express');
const router 			= express.Router();
const UserController 	= require('../controllers/user.controller');
const custom 	        = require('./../middleware/custom');
const passport      	= require('passport');
const path              = require('path');

require('./../middleware/passport')(passport)
/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({status:"success", message:"d node users 3000", data:{"version_number":"v1.0.0"}})
});

router.get(    '/call',     function(req, res, next) {
  res.json({status:"success post", message:"this is called from users.", data:{"version_number":"v1.0.0"}})
});

router.post(    '/users',           UserController.create);                                                  
router.get(     '/users',           passport.authenticate('jwt', {session:false}), UserController.get);   
router.post(     '/user_detail',   passport.authenticate('jwt', {session:false}), UserController.get);             
router.post(    '/users/login',     UserController.login);//for user login


//********* API DOCUMENTATION **********
router.use('/docs/api.json',            express.static(path.join(__dirname, '/../public/v1/documentation/api.json')));
router.use('/docs',                     express.static(path.join(__dirname, '/../public/v1/documentation/dist')));
module.exports = router;
