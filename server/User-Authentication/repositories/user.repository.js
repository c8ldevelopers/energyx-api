const { User } 	    = require('../models');

module.exports.userCreate = function(userInfo){ // Success Web Response
    return User.create(userInfo);
};

module.exports.findOne = function(userInfo){ // Success Web Response
    return User.findOne(userInfo);
};

