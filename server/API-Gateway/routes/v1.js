const express 			= require('express');
const router 			= express.Router();
const path              = require('path');
const request = require('request-promise-native');
const { to, ReE, ReS }  = require('../services/util.service');
const passport      	= require('passport');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({status:"success", message:"Gateway", data:{"version_number":"v1.0.0"}})
});
module.exports = router;

/* User login and register router */
router.post(    '/register',     async (req, res) => {
  const uri = 'http://localhost:3000/v1/users'
  request.post(
	    uri,
	    { json: req.body },
	    function (error, response, body) {
	        return res.json(response);
	    }
	);
});

router.post(    '/login',     async (req, res) => {
  const uri = 'http://localhost:3000/v1/users/login'
  request.post(
	    uri,
	    { json: req.body },
	    function (error, response, body) {
	        return res.json(response);
	    }
	);
});

/* Customer Deal and Deal detail router  */
router.get(		'/customer/deal_list', 		function(req, res) {
   		const url = 'http://localhost:3000/v1/users';
	    const auth =  req.headers['authorization'];
	    const headers = { 
		    'Authorization':auth,
		    'Content-Type' : 'application/json' 
		};
	    request.get({ url: url,  headers: headers },  function (error, response) {

	    			if(error || response.statusCode === 401 || response.body === 'Unauthorized') {		
    						return res.json(response);
    				} else {
								var userdata  = JSON.parse(response.body);
			    			 	if(userdata['user']['id'] != '') {
			    			 	 	const user_id = userdata['user']['id'];
			    			 	 	const headers =   { 'user_id':user_id };
					    			const url = 'http://localhost:4000/v1/deals';
									request.get({ url: url,  headers: headers },  function (error, response) {
											return res.json(JSON.parse(response.body));
									});
								}	
					}					   			 	
		});
});

router.post(    '/customer/deal_detail',     async (req, res) => {
	const url = 'http://localhost:3000/v1/user_detail';
    const auth =  req.headers['authorization'];
    const headers = { 
	    'Authorization':auth,
	    'Content-Type' : 'application/json' 
	};
    request.post({ url: url,  headers: headers },  function (error, response) {
    		if(error || response.statusCode === 401 || response.body === 'Unauthorized') {		
					return res.json(response);
			} else {
    			 	 var userdata  = JSON.parse(response.body);
    			 	 if(userdata['user']['id'] != '') {
    			 	 	const uri = 'http://localhost:4000/v1/deals/deal_detail';
    			 	 	const user_id = userdata['user']['id'];
    			 	 	const headers =   { 'user_id':user_id };
						request.post(
							 uri,
								    { json: req.body, headers:headers},
								    function (error, response, body) {
								        return res.json(response.body);
								    }
							);
					}
			} 
	});
});

/* Customer complete profile and product selection router */
router.post(    '/customers/complete_profile',     async (req, res) => {
	const url = 'http://localhost:3000/v1/user_detail';
    const auth =  req.headers['authorization'];
    const headers = { 
	    'Authorization':auth,
	    'Content-Type' : 'application/json' 
	};
    request.post({ url: url,  headers: headers },  function (error, response) {
    				if(error || response.statusCode === 401 || response.body === 'Unauthorized') {		
    						return res.json(response);
    				} else {	 
	    			 	 var userdata  = JSON.parse(response.body);
	    			 	 if(userdata['user']['id'] != '') {
	    			 	 	const uri = 'http://localhost:2000/v1/customers';
	    			 	 	const user_id = userdata['user']['id'];
	    			 	 	const headers =   { 'user_id':user_id };
							request.post(
								 uri,
									    { json: req.body, headers:headers},
									    function (error, response, body) {
									        return res.json(response);
									    }
								);
						} 
					}
	});
});

router.post(    '/customers/product_selection',     async (req, res) => {
	const url = 'http://localhost:3000/v1/user_detail';
    const auth =  req.headers['authorization'];
    const headers = { 
	    'Authorization':auth,
	    'Content-Type' : 'application/json' 
	};
    request.post({ url: url,  headers: headers },  function (error, response) {

    				if(error || response.statusCode === 401 || response.body === 'Unauthorized') {		
    						return res.json(response);
    				} else {
		    			 	 var userdata  = JSON.parse(response.body);
		    			 	 if(userdata['user']['id'] != '') {
		    			 	 	const uri = 'http://localhost:2000/v1/customers/products';
		    			 	 	const user_id = userdata['user']['id'];
		    			 	 	const headers =   { 'user_id':user_id };
								request.post(
									 uri,
										    { json: req.body, headers:headers},
										    function (error, response, body) {
										        return res.json(response);
										    }
									);
							} 
					}
	});
});

/* supplier complete profile and product offering router */
router.post(    '/suppliers/complete_profile',     async (req, res) => {
	const url = 'http://localhost:3000/v1/user_detail';
    const auth =  req.headers['authorization'];
    const headers = { 
	    'Authorization':auth,
	    'Content-Type' : 'application/json' 
	};
    request.post({ url: url,  headers: headers },  function (error, response) {

    				if(error || response.statusCode === 401 || response.body === 'Unauthorized') {		
    						return res.json(response);
    				} else {
		    			 	 var userdata  = JSON.parse(response.body);
		    			 	 if(userdata['user']['id'] != '') {
		    			 	 	const uri = 'http://localhost:2000/v1/suppliers';
		    			 	 	const user_id = userdata['user']['id'];
		    			 	 	const headers =   { 'user_id':user_id };
								request.post(
									 uri,
										    { json: req.body, headers:headers},
										    function (error, response, body) {
										        return res.json(response);
										    }
									);
							} 
					}
	});
});

router.post(    '/suppliers/product_offering',     async (req, res) => {
	const url = 'http://localhost:3000/v1/user_detail';
    const auth =  req.headers['authorization'];
    const headers = { 
	    'Authorization':auth,
	    'Content-Type' : 'application/json' 
	};
    request.post({ url: url,  headers: headers },  function (error, response) {

    				if(error || response.statusCode === 401 || response.body === 'Unauthorized') {		
    						return res.json(response);
    				} else {
		    			 	 var userdata  = JSON.parse(response.body);
		    			 	 if(userdata['user']['id'] != '') {
		    			 	 	const uri = 'http://localhost:2000/v1/suppliers/product_offering';
		    			 	 	const user_id = userdata['user']['id'];
		    			 	 	const headers =   { 'user_id':user_id };
								request.post(
									 uri,
										    { json: req.body, headers:headers},
										    function (error, response, body) {
										        return res.json(response);
										    }
									);
							} 
					}
	});
});

/*  Supplier Deal add list and view router */
router.post(    '/suppliers/deal_add',     async (req, res) => {
	const url = 'http://localhost:3000/v1/user_detail';
    const auth =  req.headers['authorization'];
    const headers = { 
	    'Authorization':auth,
	    'Content-Type' : 'application/json' 
	};
    request.post({ url: url,  headers: headers },  function (error, response) {
    			if(error || response.statusCode === 401 || response.body === 'Unauthorized') {		
    						return res.json(response);
    			} else {
    			 	 var userdata  = JSON.parse(response.body);
    			 	 if(userdata['user']['id'] != '') {
    			 	 	const uri = 'http://localhost:4000/v1/supplier/deal_add';
    			 	 	const user_id = userdata['user']['id'];
    			 	 	const headers =   { 'user_id':user_id };
						request.post(
							 uri,
								    { json: req.body, headers:headers},
								    function (error, response, body) {
								        return res.json(response);
								    }
							);
					} 
				}
	});
});

router.get(		'/suppliers/deal_list', 		function(req, res) {
   		const url = 'http://localhost:3000/v1/users';
	    const auth =  req.headers['authorization'];
	    const headers = { 
		    'Authorization':auth,
		    'Content-Type' : 'application/json' 
		};
	    request.get({ url: url,  headers: headers },  function (error, response) {
	    			if(error || response.statusCode === 401 || response.body === 'Unauthorized') {		
    						return res.json(response);
    				} else {
	    			 	 var userdata  = JSON.parse(response.body);
	    			 	 if(userdata['user']['id'] != '') {
	    			 	 	const user_id = userdata['user']['id'];
	    			 	 	const headers =   { 'user_id':user_id };
			    			const url = 'http://localhost:4000/v1/supplier/deals';
							request.get({ url: url,  headers: headers },  function (error, response) {
									return res.json(JSON.parse(response.body));
							});
						}
					}
		});
});

router.post(    '/suppliers/deal_detail',     async (req, res) => {
	const url = 'http://localhost:3000/v1/user_detail';
    const auth =  req.headers['authorization'];
    const headers = { 
	    'Authorization':auth,
	    'Content-Type' : 'application/json' 
	};
    request.post({ url: url,  headers: headers },  function (error, response) {
    			if(error || response.statusCode === 401 || response.body === 'Unauthorized') {		
						return res.json(response);
				} else {
    			 	 var userdata  = JSON.parse(response.body);
    			 	 if(userdata['user']['id'] != '') {
    			 	 	const uri = 'http://localhost:4000/v1/supplier/deal_detail';
    			 	 	const user_id = userdata['user']['id'];
    			 	 	const headers =   { 'user_id':user_id };
						request.post(
							 uri,
								    { json: req.body, headers:headers},
								    function (error, response, body) {
								        return res.json(response.body);
								    }
							);
					} 
				}
	});
});