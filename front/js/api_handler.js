
$( document ).ready(function() {

	$('#signup_btn').on('click',function(){
		
		var sendInfo = {
			full_name: $('#full_name').val(),
			email: $('#email').val(),
			password: $('#password').val(),
		};

		$.ajax({
			type: "POST",
			url: "http://localhost:3000/v1/users",
			dataType: "json",
			success: function (res) {
				if(res.success){
					alert('User registered successfully. Please login to continue.');
					window.location = 'signin.html';
				}else{
					alert(res.error);
				}   
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if(xhr.status == '422'){
					alert('user already exists with that email');	
				}
			},
			data: sendInfo
		});
	});

	$('#singin_btn').on('click',function(){
		
		var sendInfo = {
			email: $('#email').val(),
			password: $('#password').val(),
		};
		
		$.ajax({
			type: "POST",
			url: "http://localhost:3000/v1/users/login",
			dataType: "json",
			success: function (res) {
				if(res.success){
					$.cookie("username", res.user.full_name);
					window.location = 'company.html';
				}else{
					alert(res.error);
				}   
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if(xhr.status == '422'){
					alert('Invalid credentials.');	
				}
			},
			data: sendInfo
		});
	});

	if (typeof $.cookie('username') === 'undefined'){
	 //no cookie
	} else {
		$("#username").text($.cookie("username"));
	}
});
