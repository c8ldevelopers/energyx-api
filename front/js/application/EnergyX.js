String.prototype.hashCode = function () {
  var hash = 0, i, chr, len;
  if (this.length === 0)
    return hash;
  for (i = 0, len = this.length; i < len; i++) {
    chr = this.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }

  return (0 > hash) ? (hash * -1) : hash;
};

String.prototype.setProperty = function (key, value) {
  if (!this.hasOwnProperty("_PROP_")) {
    this._PROP_ = {};
  }
  this._PROP_[key] = value;
};
String.prototype.getProperty = function (key, defaultValue) {
  if (this.hasOwnProperty("_PROP_")) {
    if (this._PROP_.hasOwnProperty(key)) {
      return this._PROP_[key];
    } else {
      return (defaultValue === undefined) ? null : defaultValue;
    }
  } else {
    return (defaultValue === undefined) ? null : defaultValue;
  }
};


var EnergyX = (function () {
  "use strict";

  var instance;

  function init() {
    var lastVisitedPage = null;


    function start() {
      $(window).on('hashchange', function (e) {
        // alert("Stage 2="+window.location.hash);
        navigate(window.location.hash);
      });
      if (!window.location.hash) {
        Util.changeURL("index");
        return;
      }
      try {
        navigate(window.location.hash);
      } catch (exception) {
        console.log(exception);
      }
    }

    function navigate(url) {
      var requestedPage;
      // removing hash from the url
      var whereami = (window.location.hash)
        ? window.location.hash.substring(1)
        : "";

      // create param from the url
      var param = whereami.split("?");
      whereami = param[0];
      param = (param.length === 2) ? param[1] : null;
      if (param !== null) {
        param = param.split("&");
        var paramObj = {};

        for (var i = 0; i < param.length; i++) {
          var str = param[i];
          str = str.split("=");
          paramObj[str[0]] = (str.length === 2) ? str[1] : null;

        }
        param = paramObj;
      }
      requestedPage = getFromSpecialPage(whereami);

      if (requestedPage === null) {
        Util.changeURL("index");
        return;
      }
      if (lastVisitedPage !== null) {
        if (lastVisitedPage.url === whereami) {
          // returning as the rendered page is same
          return;
        }
        lastVisitedPage.page.destroy();
        lastVisitedPage = null;
      }

      try {
        requestedPage.render(param);
        lastVisitedPage = {
          url: whereami,
          page: requestedPage
        };

      } catch (exception) {
        if (exception instanceof BreakNavigationException) {

        } else {
          console.log(exception);
        }
      }
    }


    function getFromSpecialPage(whereami) {
      switch (whereami) {
        case "index":
          return new IndexPage();
        default :
          return null;
      }

    }


    return {
      // Public methods and variables
      start: start,
      navigate: navigate
    };
  }

  return {
    // Get the Singleton instance if one exists
    // or create one if it doesn't
    getInstance: function () {
      if (!instance) {
        instance = init();
      }
      return instance;
    }
  };
})();


