String.prototype.trim = function () {
  return this.replace(/^\s+|\s+$/g, "");
};

var Util = {};
(function () {
  'use strict';
  var cache = {};
  Util._storage = null;
  Util._environment = {};

  Util.reloadCurrentPage = function () {
    location.reload();
  }

  Util.setAttribute = function (key, value) {
    return Util._environment[key] = value;
  };

  Util.getAttribute = function (key, defaultAttr) {
    var retValue = Util._environment[key];
    if (retValue === undefined || retValue === null) {
      if (defaultAttr !== undefined && defaultAttr !== null)
        return defaultAttr;
    }
    return retValue;
  };


  Util.changeURL = function (URL) {
    // alert("Stage 1 = " + URL);
    window.location.hash = '#' + URL;

    // throw new BreakNavigationException();
    // if (history.pushState) {
    // // IE10, Firefox, Chrome, etc.
    // alert("Stage 1 = " + URL);
    // window.history.pushState(null, null, '#' + URL);
    // } else {
    // // IE9, IE8, etc
    // alert("WE dont support this browser");
    // }
  };

})();
