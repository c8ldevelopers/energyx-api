var LoginPage = (function () {
  "use strict";
  var instance;

  function init() {
    // Singleton
    var login = {};


    function _login() {
      // clean the error message of past if any

      var sendInfo = {
        email: $('#email').val(),
        password: $('#password').val(),
      };

      $.ajax({
        type: "POST",
        url: "http://localhost:3000/v1/users/login",
        dataType: "json",
        success: function (res) {
          if (res.success) {
            $.cookie("username", res.user.full_name);
            window.location = 'company.html';
          } else {
            alert(res.error);
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          if (xhr.status == '422') {
            alert('Invalid credentials.');
          }
        },
        data: sendInfo
      });


    }

    return {
      // Public methods and variables
      login: _login
    };
  }

  return {
    // Get the Singleton instance if one exists
    // or create one if it doesn't
    getInstance: function () {
      if (!instance) {
        instance = init();
      }
      return instance;
    }
  };
})();
